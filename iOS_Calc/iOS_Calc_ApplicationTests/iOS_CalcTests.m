#import "iOS_CalcTests.h"

@implementation iOS_CalcTests

- (void) setUp {
   app_delegate         = [[UIApplication sharedApplication] delegate];
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
   calc_view_controller = [storyboard instantiateViewControllerWithIdentifier:@"CalcViewController"];
   calc_view            = calc_view_controller.view;
}

- (void) testAppDelegate {
   XCTAssertNotNil(app_delegate, @"Cannot find the application delegate");
}
- (void) testFuntion {
   [calc_view_controller press:[calc_view viewWithTag: 6]];  // 6
   [calc_view_controller press:[calc_view viewWithTag:13]];  // +
   [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
   [calc_view_controller press:[calc_view viewWithTag:12]];  // =   
   XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"8"], @"Part 1 failed.");
   
   [calc_view_controller press:[calc_view viewWithTag:13]];  // +
   [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
   [calc_view_controller press:[calc_view viewWithTag:12]];  // =      
   XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"10"], @"Part 2 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag: 6]];  // 6
    [calc_view_controller press:[calc_view viewWithTag:14]];  // -
    [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
    [calc_view_controller press:[calc_view viewWithTag:12]];  // =
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"4"], @"");
    
    [calc_view_controller press:[calc_view viewWithTag: 6]];  // 6
    [calc_view_controller press:[calc_view viewWithTag:14]];  // -
    [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
    [calc_view_controller press:[calc_view viewWithTag:12]];  // =
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"4"], @"");
    
    [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
    [calc_view_controller press:[calc_view viewWithTag: 5]];  // 5
    [calc_view_controller press:[calc_view viewWithTag:16]];  // /
    [calc_view_controller press:[calc_view viewWithTag: 4]];  // 4
    [calc_view_controller press:[calc_view viewWithTag:12]];  // =
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"6.25"], @"");
    
    [calc_view_controller press:[calc_view viewWithTag: 1]];  // 1
    [calc_view_controller press:[calc_view viewWithTag: 9]];  // 9
    [calc_view_controller press:[calc_view viewWithTag:15]];  // x
    [calc_view_controller press:[calc_view viewWithTag: 8]];  // 8
    [calc_view_controller press:[calc_view viewWithTag:12]];  // =
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"152"], @"");
    
    [calc_view_controller press:[calc_view viewWithTag: 1]];  // 1
    [calc_view_controller press:[calc_view viewWithTag: 9]];  // 9
    [calc_view_controller press:[calc_view viewWithTag: 8]];  // 8
    [calc_view_controller press:[calc_view viewWithTag: 7]];  // 7
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"1987"], @"Part 1 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag:19]];  // D (delete)
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"198"],  @"Part 2 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag:19]];  // D (delete)
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"19"],   @"Part 3 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag:19]];  // D (delete)
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"1"],    @"Part 4 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag:19]];  // D (delete)
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"0"],    @"Part 5 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
    [calc_view_controller press:[calc_view viewWithTag: 5]];  // 5
    [calc_view_controller press:[calc_view viewWithTag:16]];  // /
    [calc_view_controller press:[calc_view viewWithTag: 4]];  // 4
    [calc_view_controller press:[calc_view viewWithTag:11]];  // C (clear)
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"0"], @"Part 1 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag: 5]];  // 5
    [calc_view_controller press:[calc_view viewWithTag:12]];  // =
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"5"], @"Part 2 failed.");
    
    [calc_view_controller press:[calc_view viewWithTag: 1]];  // 1
    [calc_view_controller press:[calc_view viewWithTag: 9]];  // 9
    [calc_view_controller press:[calc_view viewWithTag:15]];  // x
    [calc_view_controller press:[calc_view viewWithTag: 8]];  // 8
    [calc_view_controller press:[calc_view viewWithTag:11]];  // C (clear)
    [calc_view_controller press:[calc_view viewWithTag:11]];  // C (all clear)
    [calc_view_controller press:[calc_view viewWithTag:13]];  // +
    [calc_view_controller press:[calc_view viewWithTag: 2]];  // 2
    [calc_view_controller press:[calc_view viewWithTag:12]];  // =
    XCTAssertTrue([[calc_view_controller.textField text] isEqualToString:@"2"], @"Part 3 failed.");
}


@end
