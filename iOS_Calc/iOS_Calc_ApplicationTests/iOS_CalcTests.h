
#import <XCTest/XCTest.h>
#import <UIKit/UIKit.h>

#import "iOS_CalcAppDelegate.h"
#import "iOS_CalcViewController.h"

@interface iOS_CalcTests : XCTestCase {
@private
   CalcAppDelegate    *app_delegate;
   iOS_CalcViewController *calc_view_controller;
   UIView             *calc_view;
    
}

@end
