
#import <UIKit/UIKit.h>
#import "iOS_CalcAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalcAppDelegate class]));
    }
}

