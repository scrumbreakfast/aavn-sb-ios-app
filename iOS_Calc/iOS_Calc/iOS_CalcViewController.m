
#import "iOS_CalcViewController.h"

@interface iOS_CalcViewController ()
{
    NSString *Operators;
    NSString *Equals;
    NSString *Digits;
    NSString *Period;
    NSString *Delete;
    NSString *Clear;
    double operand;
    NSString *operator;
    NSMutableString *display;
    
}

@end


@implementation iOS_CalcViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    Operators = @"+-*/";
    Equals    = @"=";
    Digits    = @"0123456789.";
    Period    = @".";
    Delete    = @"D";
    Clear     = @"C";
    display = [NSMutableString stringWithCapacity:20];
    operator = nil;
}


- (IBAction)press:(id)sender
{
    NSString *tmp = [sender titleForState:UIControlStateNormal];
    static BOOL last_character_is_operator = NO;
    BOOL bad_character;
    // Does input_character contain exactly one character?
    if (!(bad_character = !(tmp && [tmp length] == 1))) {
        // Is input_character in Digits?
        if ([Digits rangeOfString: tmp].length) {
            if (last_character_is_operator) {
                // Set the display to input_character.
                [display setString: tmp];
                last_character_is_operator = NO;
            }
            // Is input_character a digit, or is a period while a period has not been added to _display?
            else if (![tmp isEqualToString: (NSString *)Period] || [display rangeOfString: (NSString *)Period].location == NSNotFound) {
                [display appendString:tmp];
            }
        }
        // Is input_character in Operators or is it Equals?
        else if ([Operators rangeOfString:tmp].length || [tmp isEqualToString:(NSString *)Equals]) {
            if (!operator && ![tmp isEqualToString:(NSString *)Equals]) {
                // input_character is this calculation's operator.
                //
                // Save the operand and the operator.
                operand  = [[display copy] doubleValue];
                operator = tmp;
            }
            else {
                // input_character is in Operators or Equals.
                //
                // Perform the computation indicated by the saved operator between the saved operand and _display.
                // Place the result in _display.
                if (operator) {
                    double operand2 = [[display copy] doubleValue];
                    switch ([Operators rangeOfString: operator].location) {
                        case 0:
                            operand = operand + operand2;
                            break;
                        case 1:
                            operand = operand - operand2;
                            break;
                        case 2:
                            operand = operand * operand2;
                            break;
                        case 3:
                            operand = operand / operand2;
                            break;
                    }
                    [display setString: [@(operand) stringValue]];
                }
                // Save the operation (if this is a chained computation).
                operator = ([tmp isEqualToString:(NSString *)Equals])? nil : tmp;
            }
            last_character_is_operator = YES;
        }
        // Is input_character Delete?
        else if ([tmp isEqualToString:(NSString *)Delete]) {
            // Remove the rightmost character from _display.
            NSInteger index_of_char_to_remove = [display length] - 1;
            if (index_of_char_to_remove >= 0) {
                [display deleteCharactersInRange:NSMakeRange(index_of_char_to_remove, 1)];
                last_character_is_operator = NO;
            }
        }
        // Is input_character Clear?
        else if ([tmp isEqualToString:(NSString *)Clear]) {
            // If there's something in _display, clear it.
            if ([display length]) {
                [display setString:[NSString string]];
            }
            // Otherwise, clear the saved operator.
            else {
                operator = nil;
            }
        }
        else {
            // input_character is an unexpected (invalid) character.
            bad_character = TRUE;
        }
    }
    if (bad_character) {
        NSException *exception = [NSException exceptionWithName:NSInvalidArgumentException
                                                         reason:@"The input_character parameter contains an unexpected value."
                                                       userInfo:@{@"arg0": tmp}];
        [exception raise];
    }
    if ([display length]) {
        self.textField.text = [display copy];
    }
    else{
        self.textField.text = @"0";
    }
}

@end
